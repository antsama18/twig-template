const {series,parallel,watch,src,dest} = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const image = require('gulp-image');
const sourcemaps = require('gulp-sourcemaps');
// const postCss = require ('gulp-postCss');
const autoprefixer = require('gulp-autoprefixer');
const babel = require('gulp-babel');
const plumber = require('gulp-plumber');
const cssnano = require('gulp-cssnano');

const paths = {
    style:{
        src:['./src/scss/styles.scss'],
        assets:'./assets/css'
    },
    js:{
        src:['./src/js/app.js'],
        assets:'./assets/js'
    },
    image:{
        src:['./src/img/**/*'],
        assets:'./assets/img'
    },
    watchFile:{
        style:'./src/scss/**/*.scss',
        js:'./src/js/**/*.js'
    }
}

function buildStyles(){
    return src(paths.style.src)
    .pipe(sourcemaps.init({loadMaps:true}))
    .pipe(sass({outputStyle:'compressed'}).on('error',sass.logError))
    .pipe(autoprefixer())
    .pipe(cssnano())
    .pipe(concat('styles.min.css'))
    .pipe(sourcemaps.write('.'))
    .pipe(dest(paths.style.assets))
}

function buildJsDev(){
    return src(paths.js.src)
    .pipe(sourcemaps.init({loadMaps:true}))
    .pipe(plumber())
    .pipe(babel({presets:[["@babel/env",{modules:false,},],],},))
    .pipe(concat('app.min.js'))
    .pipe(sourcemaps.write('.'))
    .pipe(dest(paths.js.assets))
}

function buildJsProd(){
    return src(paths.js.src)
    .pipe(sourcemaps.init({loadMaps:true}))
    .pipe(plumber())
    .pipe(babel({presets:[["@babel/env",{modules:false,},],],},))
    .pipe(uglify())
    .pipe(concat('app.min.js'))
    .pipe(sourcemaps.write('.'))
    .pipe(dest(paths.js.assets))
}

function buildImageDev(){
    return src(paths.image.src)
    .pipe(dest(paths.image.assets))
}

function buildImageProd(){
    return src(paths.image.src)
    .pipe(image())
    .pipe(dest(paths.image.assets))
}

//Exports
exports.buildStyles= buildStyles;
exports.buildJsDev= buildJsDev;
exports.buildImageDev = buildImageDev;
exports.buildImageProd = buildImageProd;


//Watch file
function watchFile(){
    watch(paths.watchFile.style,buildStyles);
    watch(paths.watchFile.js,buildJsDev);
}

//task for dev et prod
exports.default=series(buildStyles,buildJsDev,buildImageDev,watchFile);
exports.prod=series(buildStyles,buildJsProd,buildImageProd);
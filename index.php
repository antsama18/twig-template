<?php 
require 'vendor/autoload.php';

$page='main';
if(isset($_GET['p'])){
    $page=$_GET['p'];
}

// render TEMPLATE
$loader =new \Twig\Loader\FilesystemLoader('templates');
$twig = new \Twig\Environment($loader, [
    'cache' => false
]);

switch($page){
    case 'main':
    echo $twig->render('main.twig');
    break;
}
?>